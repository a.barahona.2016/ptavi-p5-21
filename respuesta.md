
## Ejercicio 3. Análisis general

* ¿Cuántos paquetes componen la captura?
  * Un total de 1090 paquetes.
* ¿Cuánto tiempo dura la captura?
  * Dura 14,491079516 segundos.
* ¿Qué IP tiene la máquina donde se ha efectuado la captura? ¿Se trata de una IP pública o de una IP privada? ¿Por qué lo sabes?
  * La IP de la maquina es 192.168.1.34. Se trata de una IP privada de clase C, que su rango comienza desde 192.168.0.0 a 192.168.255.255.
    
Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de `Statistics`. En el apartado de jerarquía de protocolos (`Protocol Hierarchy`) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
  * El primero es RTP con un 96.2% (1049 paquetes), seguido de SIP con un 0.8% (9 paquetes) y, por último, RTCP con un 0.7% (8 paquetes)
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
  * Vemos que todos los paquetes van por Ethernet, después por UDP tenemos STUN y DATA y 3 paquetes más de ICMP.
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
  * RTP con una tasa de 99000 bits/s
Observa por encima el flujo de tramas en el menú de `Statistics` en `IO Graphs`. La captura que estamos viendo incluye desde la inicialización (registro) de la aplicación hasta su finalización, incluyendo una llamada. 

* Filtra por `sip` para conocer cuándo se envían paquetes SIP. ¿En qué segundos tienen lugar esos envíos?
  * En el segundo 0 se envían 4 paquetes, en el segundo 3 se envían 3 paquetes y en el segundo 14 se envían 2 paquetes. 
* Y los paquetes con RTP, ¿cuándo se empiezan a enviar?
  * Desde el segundo 3 en adelante hasta al final de la llamada
* Los paquetes RTP, ¿cuándo se dejan de enviar?
  * En el segundo 14,4876 (paquete 1088)
* Los paquetes RTP, ¿cada cuánto se envían?
  * Uno por cada centésima de segundo

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 4. Primeras tramas

Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y "Servidor" a la máquina que proporciona el servicio SIP.

* ¿De qué protocolo de nivel de aplicación son?
  * SIP (Session Initiation Protocol)
* ¿Cuál es la dirección IP de la máquina "Linphone"?
  * 192.168.1.34
* ¿Cuál es la dirección IP de la máquina "Servidor"?
  * 212.79.111.155
* ¿En qué máquina está el UA (user agent)?
  * En la Linphone
* ¿Entre qué máquinas se envía cada trama?
  * La primera trama se envía desde la 192.168.1.34 a 212.79.111.155. La segunda trama se envía de vuelta en la dirección opuesta.
* ¿Que ha ocurrido para que se envíe la primera trama?
  * El usuario envía un REGISTER al servidor.
* ¿Qué ha ocurrido para que se envíe la segunda trama?
  * El servidor contesta al cliente con un mensaje 401 Unauthorized. Le deniega la petición de REGISTER.

Ahora, veamos las dos tramas siguientes.

* ¿De qué protocolo de nivel de aplicación son?
  * Pertenecen al protocolo SIP
* ¿Entre qué máquinas se envía cada trama?
  * La primera trama se envía desde la 192.168.1.34 a 212.79.111.155. La segunda trama se envía de vuelta en la dirección opuesta.
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
  * El cliente envía una petición REGISTER
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
  * El servidor contesta al cliente con un mensaje 200 OK a su petición de REGISTER

Ahora, las tramas 5 y 6.

* ¿De qué protocolo de nivel de aplicación son?
  * Pertenecen al protocolo SIP, más específicamente a SDP (Session Description Protocol), ya que sirve para describir la sesión.
* ¿Entre qué máquinas se envía cada trama?
  * La primera trama se envía desde la 192.168.1.34 a 212.79.111.155. La segunda trama se envía de vuelta en la dirección opuesta.
* ¿Que ha ocurrido para que se envíe la primera de ellas (quinta trama en la captura)?
  * El cliente envía una peticion de INVITE.
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (sexta trama en la captura)?
  * El servidor contesta con un mensaje de 200 OK a la petición de invitación a la sesión.
* ¿Qué se indica (en líneas generales) en la parte SDP de cada una de ellas?
  * La dirección desde la que se envía el paquete, nombre de usuario, nombre y tipo de la sesión, tiempo de la sesión, tipo de contenido que se envía.

Después de la trama 6, busca la primera trama SIP.

* ¿Qué trama es?
  * Es el paquete 11.
* ¿De qué máquina a qué máquina va?
  * Va desde la 192.168.1.34 a 212.79.111.155 
* ¿Para qué sirve?
  * Es un ACK de respuesta al mensaje anterior de 200 OK. Asienta que todo está bien.
* ¿Puedes localizar en ella qué versión de Linphone se está usando?
  * La Linphone/3.12.0. Se indica en el User-Agent.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 5. Tramas especiales

Las tramas 7 y 9 parecen un poco especiales:

* ¿De que protocolos son (indica todos los protocolos relevantes por encima de IP).
  * Pertenecen al protocolo STUN (Session Trasversal Utilities for NAT)
  * Por encima de IP está UDP con STUN, SIP, RTP, RTCP y Data. También está ICMP con RTTP.
* De qué máquina a qué máquina van?
  * Van de la 192.168.34 (cliente) a la 212.79.111.155 (servidor)
* ¿Para qué crees que sirven?
  * Le sirven al cliente para encontrar su IP pública, el tipo de NAT y el puerto de internet asociado a esa NAT.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 6. Registro

Repasemos ahora qué está ocurriendo (en lo que al protocolo SIP se refiere) en las primeras tramas SIP que hemos visto ya:

* ¿Qué dirección IP tiene el servidor que actúa como registrar SIP? ¿Por qué?
  * La dirección IP del servidor es 212.79.111.155. Tiene esta dirección porque nos conectamos a él a través de Internet y es un IP de dominio público.
* ¿A qué puerto (del servidor Registrar) se envían los paquetes SIP?
  * Al puerto 5060
* ¿Qué método SIP utiliza el UA para registrarse?
  * El método REGISTRER
* ¿Qué diferencia fundamental ves entre la primera línea del paquete SIP que envía Linphone para registrar a un usuario, y el que hemos estado enviando en la práctica 4?
  * En la práctica 4 junto a la cabecera del REGISTER indicábamos la dirección IP y el puerto de destino. En esta línea lo que vemos es que, junto a REGISTRER, está sip:iptel.org
* ¿Por qué tenemos dos paquetes `REGISTER` en la traza?
  * Porque la primera de ellas no es autorizada para el registro, le pide nuevamente que se registre.
* ¿Por qué la cabecera `Contact` es diferente en los paquetes 1 y 3? ¿Cuál de las dos cabeceras es más "correcta", si nuestro interés es que el UA sea localizable?
  * Porque en el primer paquete lo envía al servido la IP privada de su máquina. En el tercer paquete envía su IP pública en el Contact. Para ser localizable interesa más conocer su IP pública ya que nadie más va a tener esa dirección igual.
* ¿Qué tiempo de validez se está dando para el registro que pretenden realizar los paquetes 1 y 3?
  * Para ambos se está dando 3600 segundos.

Para responder estas preguntas, puede serte util leer primero [Understanding REGISTER Method](https://blog.wildix.com/understanding-register-method/), además de tener presente lo explicado en clase.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 7. Indicación de comienzo de conversación

* Además de REGISTER, ¿podrías decir qué instrucciones SIP entiende el UA?
  * INVITE, ACK, OPTIONS, BYE, CANCEL, SUBSCRIBE, NOTIFY, PUBLISH, REFER, MESSAGE, PRACK, INFO 
* ¿En qué paquete el UA indica que quiere comenzar a intercambiar datos de audio con otro? ¿Qué método SIP utiliza para ello?
  * En el paquete 5. Utiliza el método INVITE.
* ¿Con qué dirección quiere intercambiar datos de audio el UA?
  * Con el 212.79.111.155 (servidor)
* ¿Qué protocolo (formato) está usando para indicar cómo quiere que sea la conversación?
  * Con SDP y, en Session Name: Talk, da nombre a esa sesión.
* En la indicacion de cómo quiere que sea la conversación, puede verse un campo `m` con un valor que empieza por `audio 7078`. ¿Qué indica el `7078`? ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama? ¿Qué paquetes son esos?
  * Es el puerto por el que se enviarán todos los paquetes de audio en la conversación. Es el puerto por defecto de paquetes de audio en RTP.
* En la respuesta a esta indicacion vemos un campo `m` con un valor que empieza por `audio 27138`. ¿Qué indica el `27138`?  ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama? ¿Qué paquetes son esos?
  * Es el puerto por el cual salen del servidor los paquetes de audio.
* ¿Para qué sirve el paquete 11 de la trama?
  * Es un ACK de respuesta del cliente al servidor para confirmar que todo ha llegado bien y se ha establecido bien la conexión.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 8. Primeros paquetes RTP

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el paquete 12 de la traza:

* ¿De qué máquina a qué máquina va?
  * Desde la 192.168.1.34 a 212.79.111.155
* ¿Qué tipo de datos transporta?
  * Transporta datos de audio de la llamada
* ¿Qué tamaño tiene?
  * 214 bytes
* ¿Cuántos bits van en la "carga de pago" (payload)
  * 160 bytes
* ¿Se utilizan bits de padding?
  * No, está desactivado.
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
  * Uno cada dos centésimas de segundo
* ¿Cuántos bits/segundo se envían?
  * 99000 bits/s

Y ahora, las mismas preguntas para el paquete 14 de la traza:

* ¿De qué máquina a qué máquina va?
  * Desde la 212.79.111.155 a la 192.168.1.34
* ¿Qué tipo de datos transporta?
  * Transporta datos de audio en la llamada
* ¿Qué tamaño tiene?
  * 214 bytes
* ¿Cuántos bits van en la "carga de pago" (payload)
  * 160 bytes
* ¿Se utilizan bits de padding?
  * No, está desactivado
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
  * Uno cada dos centésimas de segundo
* ¿Cuántos bits/segundo se envían?
  * 99000 bits/s

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 9. Flujos RTP

Vamos a ver más a fondo el intercambio RTP. Busca en el menú `Telephony` la opción `RTP`. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay? ¿por qué?
  * Hay dos flujos, el de cliente a servidor y el de vuelta.
* ¿Cuántos paquetes se pierden?
  * 0 paquetes se pierden, llegan todos los que se envían.
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
  * 30.83 ms
* Para el flujo desde Servidor hacia LinPhone: ¿cuál es el valor máximo del delta?
  * 59.59 ms
* ¿Qué es lo que significa el valor de delta?
  * Es la diferencia de tiempo entre el paquete actual y el anterior.
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
  * Son mayores en el flujo que va desde el cliente al servidor.
* ¿Cuáles son esos valores?
  * Max Jitter: 8.45 ms , Mean Jitter: 4.15 ms
* ¿Qué significan esos valores?
  * Es el retraso que se produce en el envío de paquetes desde el momento exacto que debe llegar
* Dados los valores de jitter que ves, ¿crees que se podría mantener una conversación de calidad?
  * Sí porque estamos hablando de un jitter de muy pocos milisegundos por lo que la calidad de la llamada es buena.

Vamos a ver ahora los valores de un paquete concreto, el paquete 17. Vamos a analizarlo en opción `Stream Analysis`:

* ¿Cuánto valen el delta y el jitter para ese paquete?
  * El delta vale 20.58 ms y el jitter vale 0.10 ms
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
  * Si tiene un jitter negativo alto llegaría más tarde
* El "skew" es negativo, ¿qué quiere decir eso?
  * Pues que llega con retraso respecto al tiempo que debería llegar

Si sigues el jitter, ves que en el paquete 78 llega a ser de 5.52:

* ¿A qué se debe esa subida desde el 0.57 que tenía el paquete 53?
  * Que hay varios paquetes que, entre el paquete 53 y 58, empiezan a llegar a llegar con más retraso del esperado, unos 10ms.

En el panel `Stream Analysis` puedes hacer `play` sobre los streams:

* ¿Qué se oye al pulsar `play`?
  * Una canción con algo de ruido de fondo.
* ¿Qué se oye si seleccionas un `Jitter Buffer` de 1, y pulsas `play`?
  * Que se escucha entrecortado porque hay pqeuetes que se pierden
* ¿A qué se debe la diferencia?
  * A que con un periodo de un segundo del jitter buffer se pierden muchos paquetes entre medias

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.
  
## Ejercicio 10. Llamadas VoIP

Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú `Telephony` seleccina el menú `VoIP calls`, y selecciona la llamada hasta que te salga el panel correspondiente:

* ¿Cuánto dura la llamada?
  * 10 segundos

Ahora, pulsa sobre `Flow Sequence`:

* Guarda el diagrama en un fichero (formato PNG) con el nombre `diagrama.png`.
* ¿En qué segundo se realiza el `INVITE` que inicia la conversación?
  * En el segundo 3.877
* ¿En qué segundo se recibe el último OK que marca su final?
  * En el segundo 14.491
  
Ahora, pulsa sobre `Play Streams`, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

* ¿Cuáles son las SSRC que intervienen?
  * Intervienen 2: 0x761f98b2 y 0xeeccf15f
* ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
  * 524
* ¿Cuántos en el sentido contrario?
  * 525
* ¿Cuál es la frecuencia de muestreo del audio?
  * 8000 Hz
* ¿Qué formato se usa para los paquetes de audio?
  * Se usa el códec de audio g711U

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 11. Captura de una llamada VoIP

Dirígete a la [web de LinPhone](https://www.linphone.org/freesip/home) con el navegador y créate una cuenta SIP.  Recibirás un correo electrónico de confirmación en la dirección que has indicado al registrarte (mira en tu carpeta de spam si no es así).
  
Lanza LinPhone, y configúralo con los datos de la cuenta que te acabas de crear. Para ello, puedes ir al menú `Ayuda` y seleccionar `Asistente de Configuración de Cuenta`. Al terminar, cierra completamente LinPhone.

* Captura una llamada VoIP con el identificador SIP `sip:music@sip.iptel.org`, de unos 15 segundos de duración. Recuerda que has de comenzar a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero `linphone-music.pcapng`. Procura que en la captura haya sólo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
* ¿Cuántos flujos tiene esta captura?
  * Tiene dos flujos, desde 212.79.111.155 a 212.128.255.47 y el flujo inverso.
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?
  * Flujo de 212.79.111.155 a 212.128.255.47:
    * Max Delta: 24.442 ms    Max Jitter: 0.620 ms    Mean Jitter: 0.245 ms  
  * Flujo de 212.128.255.47 a 212.79.111.155:
    * Max Delta: 40.447 ms    Max Jitter: 6.687 ms    Mean Jitter: 5.364 ms  
    

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.


## Ejercicio 12 (segundo periodo). Llamada LinPhone

Ponte de acuerdo con algún compañero para realizar una llamada SIP conjunta (entre los dos) usando LinPhone, de unos 15 segundos de duración.  Si quieres, usa el foro de la asignatura, en el hilo correspondiente a este ejercicio, para encontrar con quién hacer la llamada.

Realiza una captura de esa llamada en la que estén todos los paquetes SIP y RTP (y sólo los paquetes SIP y RTP). No hace falta que tu compañero realice captura (pero puede realizarla si quiere que le sirva también para este ejercicio).

Guarda tu captura en el fichero `captura-a-dos.pcapng`

Como respuesta a este ejercicio, indica con quién has realizado la llamada, quién de los dos la ha iniciado, y cuantos paquetes hay en la captura que has realizado.

  * He realizado la llamada con: sip:refferenz@212.128.255.48:5060. Siendo yo (sip:abarahon@212.128.255.47) quien ha iniciado la llamada. 
  * Hay 4 flujos de paquetes en cada dirección, 2 codificados como VP8(110+110=220 paquetes) y otros 2 codificados como opus(972+973=1945 paquetes).  

